Kfp = 0.8722; %N/V
Kfy = 0.42; %N/V
Kty = 0.01; %Nm/V
Ktp = 0.02; %Nm/V
L = 0.406;
%Vp = 1;
%Vy = 1;
Rc = 0.01; %center of gravity distance from pitch axis
FrameMass = 0.146;
YawMotorMass = 0.286; 
PitchMotorMass = 0.286;
M = FrameMass+ YawMotorMass + PitchMotorMass; %mass of center of gravity 
g = 9.81; 
Fg = M * g; 
Jpp = 0.0307; %Rotational inertia about pitch axis Kgm^2
Jyy = 0.0025; %Rotational inertia about yaw axis Kgm^2

A = [0 0 1 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
B = [0 0; 0 0; (L*Kfp)/Jpp -Kty/Jpp; -(Ktp)/Jyy (L*Kfy)/Jpp]; 
C = [1 0 0 0];
Disturbance = [0; 0; (Rc*Fg)/Jpp; 0];

T = 0:0.01:2;         % simulation time = 10 seconds
U = zeros(201,2);    % no input
X0 = [(Rc*Fg)/Jpp 0 0 0];    % initial conditions of the three states
sys = ss(A,B,C,0);     % construct a system model
lsim(sys, U, T, Disturbance)    % simulate and plot the response (the output)
title('Response to Non-Zero Initial Conditions')
