%constants
RPMtoRad = 2*pi/60;
LbinToKgM = 0.112985;
RadPerDeg = pi/180;
Gravity = 9.81;     %m/s^2
Gravity_in = 386;    %in/sec^2   Gravity in inch/s^2
SteelDensity = 0.28;    %lb/in

%Desired Trajectory
Alt = [0 0 10 10 10 10 10 10
       00 03 05 06 12 15 20 30];        %1st line = height
                                        %2nd line = time
Yaw = [0 pi pi pi
       00 05 10 30];                    

torqueGain = 0.10;   % Torque Gain [torque directed to yaw rotor from lift motor] (%)

%masses
pitchBarMass = 19.28e-3; %kg
secondarypitchBarMass = 34.63e-3; %kg
SideBarMass = 40e-3; %kg
TopDiskMass = 0.24;  %kg


%Yaw Motor
YawMotorVoltage = 12;
YawMotorEff = 0.74;
YawMotorCurrent = 0.93;
YawMotorTorque = 0.012;
YawMotorSpeed = 6468; %
YawMotorShaftDia = 0.09; %inch
YawMotorShaftLen = 0.33; %inch
YawMotorMass = 0.38; %kg
YawMotorResistance = 3.5;  %Ohm
YawMotorInductance = 1.047e-3;   %H
YawKe = YawMotorVoltage/(YawMotorSpeed*RPMtoRad);
YawKt = YawMotorTorque/YawMotorCurrent;
YawMotorDamping = 1e-8; 
YawRotorInertia = LbinToKgM*pi*YawMotorShaftLen*SteelDensity*(YawMotorShaftDia/2)^4 / (2*Gravity_in);
Radius_yaw = 10e-2;        %(cm)
PropellerPitch_yaw = 4.5;      %(in)
YawArmLength = 18.5e-2;         %(m)
PitchBarMass_yaw = 0.5*pitchBarMass + secondarypitchBarMass;   %(kg)
ThrustGain = 2.3425 / (YawMotorSpeed*RPMtoRad);     %N/(rad/s)
B2 = 0; %air friction (N/(m/s))

%Pitch Motor
PitchMotorVoltage = 12;
PitchMotorEff = 0.74;
PitchMotorCurrent = 0.93;
PitchMotorTorque = 0.012;
PitchMotorSpeed = 6468; %
PitchMotorShaftDia = 0.09; %inch
PitchMotorShaftLen = 0.33; %inch
PitchMotorMass = 0.38; %kg
PitchMotorResistance = 3.5;  %Ohm
PitchMotorInductance = 1.047e-3;   %H
PitchRotorInertia = LbinToKgM*pi*PitchMotorShaftLen*SteelDensity*(PitchMotorShaftDia/2)^4 / (2*Gravity_in);
PitchKe = PitchMotorVoltage/(PitchMotorSpeed*RPMtoRad);
PitchKt = PitchMotorTorque/PitchMotorCurrent;
PitchMotorDamping = 1e-8;
Radius_pitch = 10e-2;        %(cm)
PropellerPitch_pitch = 4.5; %(in)     
PitchArmLength = 18.5e-2;         %(m)
PitchBarMass_pitch = 0.5*pitchBarMass + 0.5*pitchBarMass + 2*secondarypitchBarMass;   %(kg)
LiftGain = 2.3425 / (PitchMotorSpeed*RPMtoRad);     %N/(rad/s)
B1 = 0; %air friction (N/(m/s))

%Helicopter
HeliLength = PitchArmLength + YawArmLength;          %(m)
TotalBarMass = PitchBarMass_pitch + PitchBarMass_yaw; %kg
CentreOfMass = (PitchMotorMass*PitchArmLength + YawMotorMass*YawArmLength)/(PitchMotorMass+YawMotorMass);
HeliFg = CentreOfMass*(TotalBarMass)*9.81;         %m * (kg/m) * m/s^-2 = kg m^2/s
HeliMass = TotalBarMass + PitchMotorMass + YawMotorMass;     %the mass of heli body
HeliMassTotal = HeliMass + 2*SideBarMass + TopDiskMass;     %heli body + sidebars + topdisk + etc 
%**********************NEEDS FIXING***********************

%effective mass
m_body_p = TotalBarMass; %mass moving about pitch axis
m_body_y = TotalBarMass + 2*SideBarMass + TopDiskMass; %mass moving about yaw axis 

%moment of inertia of heli body about its centre of mass
J_body_p = m_body_p * HeliLength^2 / 12; 
J_body_y = m_body_y * HeliLength^2 / 12;
%moment of inertia of motors about pivot
J_p = PitchMotorMass * PitchArmLength^2;
J_y = YawMotorMass * YawArmLength^2;
%equivalent moment of inertia about pitch&yaw axis
J_eq_p = PitchRotorInertia + J_body_p + J_p + J_y; %Total Moment Of Inertia about pitch axis
J_eq_y = YawRotorInertia + J_body_y + J_p + J_y; %Total Moment Of Inertia about yaw axis
%moment inertia about pitch&yaw
Jpp = J_eq_p + (TotalBarMass * CentreOfMass^2);    %heli inertia about pitch axis kg m^2
Jyy = J_eq_y + (TotalBarMass * CentreOfMass^2);    %heli inertia about yaw axis

HeliDenom = [Jpp 0 0]; 
HeliDenom2 = [Jyy 0 0];

%sensor
PSensorSens = 5/360;      %V/deg
YSensorSens = 5/360;         %V/deg
PitchSensor = PSensorSens /(PitchArmLength*sin(1));   %V/deg / m/deg = V/m
YawSensor = YSensorSens * RadPerDeg;   %V/deg * rad/deg = V/rad

UYawTorque = torqueGain;        % % (percentage)
UPitchTorque = torqueGain;     % % (Percentage)



