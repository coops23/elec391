#include "PID.h"

PID::PID()
{
    
}

void PID::Initialize(float kp, float ki, float kd, float setpoint, float errorMargin, float outputMin, float outputMax)
{
    _kp = kp;
    _ki = ki;
    _kd = kd;
    _setpoint = setpoint;
    _errorMargin = errorMargin;
    _ITerm = 0;
    _previousError = 0;
    _outputMin = outputMin;
    _outputMax = outputMax;
}

float PID::Compute(float input, unsigned long timeDelta)
{
    float error = _setpoint - input;
    float output = 0, pTerm = 0, iTerm = 0, dTerm = 0, diff = 0;
    float dt =((float)(timeDelta)) / 1000000; //Place in seconds

    //integration
    if ((_setpoint >= (input - _errorMargin)) && (_setpoint <= (input + _errorMargin)))
    {
        _ITerm = 0;
    }
    else
    {
        _ITerm += (error * dt);
        _ITerm = constrain(_ITerm, _outputMin, _outputMax); //windup guarding
    }
    
    //differentiation
    diff = (error - _previousError) / dt;
      
    //Serial.print("error: "); Serial.print(error); Serial.print(" previous error: "); Serial.print(_previousError); Serial.print(" dt: "); Serial.print(dt); Serial.print(" diff: "); Serial.println(diff);
    //scaling
    pTerm = (_kp * error);
    iTerm = (_ki * _ITerm);
    dTerm = (_kd * diff);

    //output
    output = constrain(pTerm + iTerm + dTerm, _outputMin, _outputMax);
    
    //Save the current error as previous error for next iteration
    _previousError = error;

    return output;
}

void PID::SetKp(float value)
{
    _kp = value;
}

void PID::SetKi(float value)
{
    _ki = value;
}

void PID::SetKd(float value)
{
    _kd = value;
}

void PID::SetSetpoint(float value)
{
    _setpoint = value;  
}

void PID::SetErrorMargin(float value)
{
    _errorMargin = value;
}

float PID::GetKp()
{
    return _kp;  
}

float PID::GetKi()
{
    return _ki;  
}

float PID::GetKd()
{
    return _kd;  
}

float PID::GetSetpoint()
{
    return _setpoint;
}

float PID::GetErrorMargin()
{
    return _errorMargin;
}
