#include "Motors.h"

Motors::Motors()
{
    
}

void Motors::Initialize(int pitchMotorPos, int pitchMotorNeg, int yawMotorPos, int yawMotorNeg)
{
    int i = 0;
    
    _pins[0] = pitchMotorPos;
    _pins[1] = pitchMotorNeg;
    _pins[2] = yawMotorPos;
    _pins[3] = yawMotorNeg;
    
    for(i = 0; i< sizeof(_pins); i++)
    {
       pinMode(_pins[i], OUTPUT);
       analogWrite(_pins[i], MOTOR_STOP);
    }
 
    _pitchSpeed = MOTOR_STOP;
    _yawSpeed = MOTOR_STOP;
    _pitchDirection = FORWARD;
    _yawDirection = FORWARD;
}

void Motors::Move()
{    
    analogWrite(_pitchDirection ? _pins[0] : _pins[1], _pitchSpeed);
    analogWrite(_pitchDirection ? _pins[1] : _pins[0], MOTOR_STOP);
    analogWrite(_yawDirection ? _pins[2] : _pins[3], _yawSpeed);
    analogWrite(_yawDirection ? _pins[3] : _pins[2], MOTOR_STOP);
}

void Motors::SetMotors(unsigned char pitchMotorSpeed, boolean pitchMotorDirection, unsigned char yawMotorSpeed, boolean yawMotorDirection)
{
    _pitchSpeed = pitchMotorSpeed;
    _yawSpeed = yawMotorSpeed;
    _pitchDirection = pitchMotorDirection;
    _yawDirection = yawMotorDirection; 
}

void Motors::SetPitchSpeed(unsigned char value)
{
    _pitchSpeed = value;
}

void Motors::SetYawSpeed(unsigned char value)
{
    _yawSpeed = value;
}

void Motors::SetPitchDirection(boolean value)
{
    _pitchDirection = value;
}

void Motors::SetYawDirection(boolean value)
{
    _yawDirection = value;
}
