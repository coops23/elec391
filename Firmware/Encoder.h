#include <Arduino.h>

#define DATA    (0)
#define MUX_SELA    (1)
#define MUX_SELB    (2)
#define MUX_SELC    (3)
#define SEL1    (4)
#define SEL2    (5)
#define OEN    (6)
#define RESET    (7)
#define IR_GATE_RESET  (8)

#define DEGREE_PER_COUNT (0.9) //360/400

class Encoder {
  public:
    Encoder ();
    void Initialize (int dataPin, int  muxSelectPins[3], int selectPins[2], int oen, int reset, int irGateInterrupt);
    long GetCount ();
    double GetDegree();
    
    void Reset ();
  private:
    int pins[9];
    
    unsigned char ReadRegister ();
};

