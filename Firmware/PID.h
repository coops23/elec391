#include <Arduino.h>

class PID {
  public:
    PID ();
 
    void Initialize(float kp, float ki, float kd, float setpoint, float errorMargin, float outputMin, float outputMax);

    float Compute(float input, unsigned long timeDelta);
    
    void SetKp(float value);
    void SetKi(float value);
    void SetKd(float value);
    void SetSetpoint(float value);
    void SetErrorMargin(float value);
    
    float GetKp();
    float GetKi();
    float GetKd();
    float GetSetpoint();
    float GetErrorMargin();
    
  private:
    float _kp;
    float _ki;
    float _kd;
    float _setpoint;
    float _errorMargin;
    float _ITerm;
    float _previousError;
    float _outputMin;
    float _outputMax;
};

    
