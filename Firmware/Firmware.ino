#include "Encoder.h"
#include "Motors.h"
#include "PID.h"

//#define OSCILLATOR_CORRECTION (4/3) //Used with serial, delay, micros, millis too correct for using 12MHz oscillator rather than 16MHz

#define DATA    (4) //pin6 or arduinouno pin4
#define MUX_SELA    (7) //pin13 or arduino uno pin7
#define MUX_SELB    (8) //pin14 or arduino uno pin8
#define MUX_SELC    (A5) //pin28 or arduino uno analog5
#define SEL1    (A4) //pin27 or arduino uno analog4
#define SEL2    (A3) //pin 26 or arduino uno analog3
#define OEN    (A2) //pin26 or arduino uno analog2
#define RESET    (A1) //pin25 or arduino uno analog1
#define IR_GATE_RESET  (0) //pin4 or arduino uno pin2

#define PITCH_MOTOR_POS (6) //pin5 or arduino uno pin3
#define PITCH_MOTOR_NEG (11) //pin11 or arduino uno pin5
#define YAW_MOTOR_POS (3)
#define YAW_MOTOR_NEG (5)

#define POT_PIN  (A0) //pin24 or arduino uno analog0

#define PITCH_NEW_MOTOR_SPEED (9)
#define PITCH_NEW_MOTOR_DIRECTION (10)

#define TIME_DELTA_TEST_PIN (13) 

#define MOTOR_COMMAND (1)
#define ENCODER_READ_COMMAND (2)
#define POTENTIOMETER_READ_COMMAND (3)
#define GET_PITCH_INFO (4)
#define SET_PITCH_KP (5)
#define SET_PITCH_KI (6)
#define SET_PITCH_KD (7)
#define SET_PITCH (8)
#define GET_YAW_INFO (9)
#define SET_YAW_KP (10)
#define SET_YAW_KI (11)
#define SET_YAW_KD (12)
#define SET_YAW (13)
#define GET_YAW_PITCH (14) 

#define PITCH_KP_VALUE (5)
#define PITCH_KI_VALUE (2.5)
#define PITCH_KD_VALUE (0.3)
#define PITCH_INITAL_SETPOINT (90)
#define PITCH_AXIS_ERROR_MARGIN (2)
#define PITCH_AXIS_TOP_SPEED (255)

#define YAW_KP_VALUE (1.5)
#define YAW_KI_VALUE (2.5)
#define YAW_KD_VALUE (7)
#define YAW_INITIAL_SETPOINT (720)
#define YAW_AXIS_ERROR_MARGIN (1)
#define YAW_AXIS_TOP_SPEED (180)

void ResetEncoder();
void CommandHandler();

Encoder encoder;
Motors motors;
PID pitchPid;
PID yawPid;

unsigned long before = 0;
unsigned long now = 0; 
unsigned long timeDelta = 0;

long encoderOneCount = 0;
int pitchAxisValue = 0;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

void ResetEncoder()
{
  encoder.Reset();
  yawPid.SetSetpoint(YAW_INITIAL_SETPOINT);
}

/*
Uses 32 bit number codes do perform commands

Command  status   value    value
xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx

Motor Move
Send
[31:24] = 1  [23:18] = DC, [17] = yaw direction, [16]= pitch direction, [15:8] = yaw speed  [7:0] = pitch speed
0 is forward, 1 is backwards, 0-255 for speed (255 being fast)

Get Count Encoder One
Send
[31:0] = 2
Returns
[31:0] = encoder one count

Get Potentiometer Value
Send
[31:0] = 3
Returns
[31:0] = value

Get time delta
Send 
[31:0] = 4
Returns
[31:0] = time delta in micro seconds
*/

void CommandHandler()
{
  if (stringComplete) {
    char msg[256];
    char* ptr;
    unsigned long command = 0;
    unsigned char byte3 = 0, byte2 = 0, byte1 = 0, byte0 = 0;
    
    inputString.toCharArray(msg, sizeof(msg));
    command = strtoul(msg, &ptr, 10);
    byte3 = (command & 0xFF000000) >> 24;
    byte2 = (command & 0x00FF0000) >> 16;
    byte1 = (command & 0x0000FF00) >> 8;
    byte0 = (command & 0x000000FF);
    
    switch(byte3)
    {
        case MOTOR_COMMAND:
          motors.SetMotors(byte0, (byte2 & 0x01), byte1, (byte2 & 0x02) >> 1); 
          break;
        case ENCODER_READ_COMMAND:
          Serial.println(encoderOneCount);
          break;
        case POTENTIOMETER_READ_COMMAND:
          Serial.println(pitchAxisValue);
          break;
        case GET_PITCH_INFO:
          Serial.print(pitchPid.GetKp(),6);Serial.print(",");Serial.print(pitchPid.GetKi(),6);Serial.print(",");Serial.print(pitchPid.GetKd(),6);Serial.print(",");Serial.println(timeDelta);
          break;
        case SET_PITCH_KP:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000000;

          pitchPid.SetKp(value);
          break;
        }
        case SET_PITCH_KI:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000000;
          
          pitchPid.SetKi(value);
          break;
        }
        case SET_PITCH_KD:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000000;
          
          pitchPid.SetKd(value);
          break;
        }
        case SET_PITCH:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000;
          
          pitchPid.SetSetpoint(value);
          break;
        }
        case GET_YAW_INFO:
          Serial.print(yawPid.GetKp(),6);Serial.print(",");Serial.print(yawPid.GetKi(),6);Serial.print(",");Serial.print(yawPid.GetKd(),6);Serial.print(",");Serial.println(timeDelta);
          break;
        case SET_YAW_KP:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000000;

           yawPid.SetKp(value);
          break;
        }
        case SET_YAW_KI:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000000;
          
          yawPid.SetKi(value);
          break;
        }
        case SET_YAW_KD:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000000;
          
          yawPid.SetKd(value);
          break;
        }
        case SET_YAW:
        {
          unsigned long temp = (unsigned long)(command & 0x00FFFFFF);
          float value = (float)temp / 1000;
          
          yawPid.SetSetpoint(value);
          break;
        }
        case GET_YAW_PITCH:
          Serial.print(pitchPid.GetSetpoint(),3);Serial.print(",");Serial.println(yawPid.GetSetpoint(),3);
          break;
        default:
          break;  
    }
    
    // clear the string:
    inputString = "";
    stringComplete = false;
  }  
}

void setup()
{  
  int muxSelectPins[3] = {MUX_SELA, MUX_SELB, MUX_SELC};
  int selectPins[2] = {SEL1, SEL2};

  motors.Initialize(PITCH_MOTOR_POS, PITCH_MOTOR_NEG, YAW_MOTOR_POS, YAW_MOTOR_NEG);
  encoder.Initialize(DATA, muxSelectPins, selectPins, OEN, RESET, IR_GATE_RESET);
  pitchPid.Initialize(PITCH_KP_VALUE, PITCH_KI_VALUE, PITCH_KD_VALUE, PITCH_INITAL_SETPOINT, PITCH_AXIS_ERROR_MARGIN, -255, 255);
  yawPid.Initialize(YAW_KP_VALUE, YAW_KI_VALUE, YAW_KD_VALUE, YAW_INITIAL_SETPOINT, YAW_AXIS_ERROR_MARGIN, -180, 180);
  
  pinMode(PITCH_NEW_MOTOR_SPEED, OUTPUT);
  pinMode(PITCH_NEW_MOTOR_DIRECTION, OUTPUT);
  
  //attachInterrupt NEEDS to be at the of the method
  attachInterrupt(IR_GATE_RESET, ResetEncoder, RISING);
  pinMode(TIME_DELTA_TEST_PIN, OUTPUT);
  digitalWrite(TIME_DELTA_TEST_PIN, LOW);
  motors.Move();
   
  //Serial.begin(12800); //actually 9600! 
  Serial.begin(9600);
  inputString.reserve(200);
}

void loop()
{ 
  int pinState = 0;
  float pitchOutput = 0, yawOutput = 0;
  float pitchAxisValueTemp = 0, yawAxisValueTemp = 0;
  float pitchOutputFinal = 0, yawOutputFinal = 0;
  int pitchAxisValueAccumulator = 0;
  
  encoderOneCount = encoder.GetCount();
  yawAxisValueTemp = (float)encoderOneCount;
  yawAxisValueTemp = yawAxisValueTemp *0.9;
  yawAxisValueTemp += YAW_INITIAL_SETPOINT;
  
  for(int i = 0; i < 10; i++)
  {
      pitchAxisValueAccumulator += analogRead(POT_PIN);
  }
  
  pitchAxisValue = pitchAxisValueAccumulator/10;
  
  /*pitchAxisValueAccumulator = analogRead(POT_PIN);
  for(int i = 0; i < 10; i ++)
  {
    pitchAxisValue = (0.4*analogRead(POT_PIN)) + ((1-0.4)*pitchAxisValueAccumulator);
  }*/
  pitchAxisValue = analogRead(POT_PIN);
  pitchAxisValueTemp = (float)pitchAxisValue;
  pitchAxisValueTemp = pitchAxisValueTemp * 280 / 1023;
  //Serial.println(pitchAxisValueTemp);
  now = micros();
  timeDelta = now - before;
  
  pitchOutput = pitchPid.Compute(pitchAxisValueTemp, timeDelta);
  yawOutput = yawPid.Compute(yawAxisValueTemp, timeDelta);
  //Serial.println(pitchOutput);
  //Serial.print(pitchAxisValueTemp); Serial.print(","); Serial.print(pitchOutput); Serial.print(","); Serial.print((unsigned char) fabs(pitchOutput)); Serial.print(","); Serial.println((pitchOutput < 0) ? 1: 0);
  motors.SetMotors((unsigned char)fabs(pitchOutput), (pitchOutput < 0) ? 1 : 0, (unsigned char)fabs(yawOutput), (yawOutput < 0) ? 1 : 0);
  analogWrite(PITCH_NEW_MOTOR_SPEED, (unsigned char)fabs(pitchOutput));
  digitalWrite(PITCH_NEW_MOTOR_DIRECTION, (pitchOutput < 0) ? 1 : 0);
  
  motors.Move();
  CommandHandler();
  
  before = now;
  
  //Used to pin the true loop time delta, simple multiply the frequency probed by 2
  pinState = !digitalRead(TIME_DELTA_TEST_PIN);
  digitalWrite(TIME_DELTA_TEST_PIN, pinState);
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}

