#include <Arduino.h>

//Values mapped on arduino Mega 2560
#define MOTOR_MIN_INPUT (0)
#define MOTOR_MAX_INPUT (255)

#define MOTOR_STOP (0)

#define FORWARD (0)
#define BACKWARD (1)

class Motors {
  public:
    Motors ();
    
    void Initialize(int pitchMotorPos, int pitchMotorNeg, int yawMotorPos, int yawMotorNeg);
    
    void Move();
    
    void SetMotors(unsigned char pitchMotorSpeed, boolean pitchMotorDirection, unsigned char yawMotorSpeed, boolean yawMotorDirection);
    void SetPitchSpeed(unsigned char value);
    void SetYawSpeed(unsigned char value);
    void SetPitchDirection(boolean value);
    void SetYawDirection(boolean value);
    
  private:
    int _pins[4];
    unsigned char _pitchSpeed;
    unsigned char _yawSpeed;
    boolean _pitchDirection;
    boolean _yawDirection;
};

