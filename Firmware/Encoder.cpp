#include "Encoder.h"

Encoder::Encoder () {}

void Encoder::Initialize(int dataPin, int  muxSelectPins[3], int selectPins[2], int oen, int reset, int irGateInterrupt) 
{
    int i = 0;
    
    pins[DATA] = dataPin;
    pins[MUX_SELA] = muxSelectPins[0];
    pins[MUX_SELB] = muxSelectPins[1];
    pins[MUX_SELC] = muxSelectPins[2];
    pins[SEL1] = selectPins[0];
    pins[SEL2] = selectPins[1];
    pins[OEN] = oen;
    pins[RESET] = reset;
    pins[IR_GATE_RESET] = irGateInterrupt;
    
    pinMode(pins[DATA], INPUT);
    pinMode(pins[MUX_SELA], OUTPUT);
    pinMode(pins[MUX_SELB], OUTPUT);
    pinMode(pins[MUX_SELC], OUTPUT);
    pinMode(pins[SEL1], OUTPUT);
    pinMode(pins[SEL2], OUTPUT);
    pinMode(pins[OEN], OUTPUT);
    pinMode(pins[RESET], OUTPUT);

    Reset();
}

long Encoder::GetCount()
{
    unsigned long value = 0;

    digitalWrite(pins[OEN], HIGH);

    digitalWrite(pins[SEL1], LOW);
    digitalWrite(pins[SEL2], HIGH);
    digitalWrite(pins[OEN], LOW);
    value = ReadRegister() << 24;

    digitalWrite(pins[SEL1], HIGH);
    digitalWrite(pins[SEL2], HIGH);
    value += ReadRegister() << 16;

    digitalWrite(pins[SEL1], LOW);
    digitalWrite(pins[SEL2], LOW);
    value += ReadRegister() << 8;

    digitalWrite(pins[SEL1], HIGH);
    digitalWrite(pins[SEL2], LOW);
    value += ReadRegister();

    digitalWrite(pins[OEN], HIGH);

    return (long)value; 
}

//Not accurate until the encoders have been reset
double Encoder::GetDegree()
{
    return ((double)GetCount() * DEGREE_PER_COUNT);
}

void Encoder::Reset()
{
    digitalWrite(pins[SEL1], LOW);
    digitalWrite(pins[SEL2], LOW);
    digitalWrite(pins[OEN], HIGH);
    digitalWrite(pins[RESET], LOW);
    delay(133);
    digitalWrite(pins[RESET], HIGH);
    detachInterrupt(pins[IR_GATE_RESET]);
}

unsigned char Encoder::ReadRegister()
{
    unsigned char value = 0;

    digitalWrite(pins[MUX_SELA], LOW);
    digitalWrite(pins[MUX_SELB], LOW);
    digitalWrite(pins[MUX_SELC], LOW);
    value = digitalRead(pins[DATA]);
    
    digitalWrite(pins[MUX_SELA], HIGH);
    digitalWrite(pins[MUX_SELB], LOW);
    digitalWrite(pins[MUX_SELC], LOW);
    value += digitalRead(pins[DATA]) << 1;
    
    digitalWrite(pins[MUX_SELA], LOW);
    digitalWrite(pins[MUX_SELB], HIGH);
    digitalWrite(pins[MUX_SELC], LOW);
    value += digitalRead(pins[DATA]) << 2;
    
    digitalWrite(pins[MUX_SELA], HIGH);
    digitalWrite(pins[MUX_SELB], HIGH);
    digitalWrite(pins[MUX_SELC], LOW);
    value += digitalRead(pins[DATA]) << 3;
    
    digitalWrite(pins[MUX_SELA], LOW);
    digitalWrite(pins[MUX_SELB], LOW);
    digitalWrite(pins[MUX_SELC], HIGH);
    value += digitalRead(pins[DATA]) << 4;
    
    digitalWrite(pins[MUX_SELA], HIGH);
    digitalWrite(pins[MUX_SELB], LOW);
    digitalWrite(pins[MUX_SELC], HIGH);
    value += digitalRead(pins[DATA]) << 5;
    
    digitalWrite(pins[MUX_SELA], LOW);
    digitalWrite(pins[MUX_SELB], HIGH);
    digitalWrite(pins[MUX_SELC], HIGH);
    value += digitalRead(pins[DATA]) << 6;
    
    digitalWrite(pins[MUX_SELA], HIGH);
    digitalWrite(pins[MUX_SELB], HIGH);
    digitalWrite(pins[MUX_SELC], HIGH);
    value += digitalRead(pins[DATA]) << 7;
    
    return value;
}
