import sys, serial, time, ctypes
from PyQt4 import QtCore, QtGui
from main_qt import Ui_MainWindow

COMMAND_PORT = "/dev/ttyUSB0"

class Main(QtGui.QMainWindow):
    def __init__(self):
        super(Main, self).__init__()
        
        self.serial = serial.Serial(port=COMMAND_PORT, baudrate=9600, timeout=None)

        # build ui
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # connect signals
        self.ui.motorButton.clicked.connect(self.Motors)
        self.ui.angularPositionButton.clicked.connect(self.GetEncoderInfo)
        self.ui.pidPitchSetButton.clicked.connect(self.SetPitchPidInfo)
        self.ui.pidPitchGetInfoButton.clicked.connect(self.GetPitchPidInfo)
        self.ui.pidYawSetButton.clicked.connect(self.SetYawPidInfo)
        self.ui.pidYawGetInfoButton.clicked.connect(self.GetYawPidInfo)
        self.ui.moveButton.clicked.connect(self.SetPosition)
        self.ui.demoButton.clicked.connect(self.Demo)
        self.ui.initializeButton.clicked.connect(self.Initialize)
    def Motors(self):
        command = 0x01000000
        command += self.ui.motorOneSpinBox.value()
        command += self.ui.motorTwoSpinBox.value() << 8
        command = str(command)
        command += "\n" 
        self.serial.write(command)

    def GetEncoderInfo(self):
        command = 0x02000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        response = self.serial.readline()
        self.ui.encoderOneCount.setText(response)
        self.ui.encoderOneAngle.setText(str(float(response)*0.9))
        
        command = 0x03000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        response = self.serial.readline()
        self.ui.encoderTwoCount.setText(response)
        response = float(response)*280/1023
        self.ui.encoderTwoAngle.setText(str(response))

    def GetPitchPidInfo(self):
        command = 0x04000000
        command = str(command)
        command += "\n"
        self.serial.write(command)
        response = self.serial.readline()
        response = response.split(",")
        self.ui.pidInfoKpText.setText(response[0])
        self.ui.pidInfoKiText.setText(response[1])
        self.ui.pidInfoKdText.setText(response[2])
        self.ui.pidInfoTimeDeltaText.setText(response[3])
          
    def SetPitchPidInfo(self):
        command = self.ui.setKpSpinBox.value()
        command = int(command*1000000) & 0x00FFFFFF
        command += 0x05000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        
        command = self.ui.setKiSpinBox.value()
        command = int(command*1000000) & 0x00FFFFFF
        command = int(command) & 0x00FFFFFF
        command += 0x06000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)

        command = self.ui.setKdSpinBox.value()
        command = int(command*1000000) & 0x00FFFFFF
        command = int(command) & 0x00FFFFFF
        command += 0x07000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)

    def GetYawPidInfo(self):
        command = 0x09000000
        command = str(command)
        command += "\n"
        self.serial.write(command)
        response = self.serial.readline()
        response = response.split(",")
        self.ui.pidYawInfoKpText.setText(response[0])
        self.ui.pidYawInfoKiText.setText(response[1])
        self.ui.pidYawInfoKdText.setText(response[2])
        self.ui.pidYawInfoTimeDeltaText.setText(response[3])

    def SetYawPidInfo(self):
        command = self.ui.setYawKpSpinBox.value()
        command = int(command*1000000) & 0x00FFFFFF
        command += 0x0A000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        
        command = self.ui.setYawKiSpinBox.value()
        command = int(command*1000000) & 0x00FFFFFF
        command = int(command) & 0x00FFFFFF
        command += 0x0B000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)

        command = self.ui.setYawKdSpinBox.value()
        command = int(command*1000000) & 0x00FFFFFF
        command = int(command) & 0x00FFFFFF
        command += 0x0C000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)

    def SetPosition(self):
        command = self.ui.setPitchSpinBox.value()
        command = int(command*1000) & 0x00FFFFFF
        command += 0x08000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        
        command = self.ui.setYawSpinBox.value()
        command = int(command*1000) & 0x00FFFFFF
        command = int(command) & 0x00FFFFFF
        command += 0x0D000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)

        self.GetPosition()

    def GetPosition(self):
        command = 0x0E000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        response = self.serial.readline()
        print response

    def Initialize(self):
        response = ""
        yaw = ""
        pitch = ""
        
        while True:
            command = 90
            command = int(command*1000) & 0x00FFFFFF
            command += 0x08000000
            command = str(command)
            command += "\n" 
            self.serial.write(command)
            
            command = 360
            command = int(command*1000) & 0x00FFFFFF
            command = int(command) & 0x00FFFFFF
            command += 0x0D000000
            command = str(command)
            command += "\n" 
            self.serial.write(command)
            
            command = 0x0E000000
            command = str(command)
            command += "\n" 
            self.serial.write(command)
            response = self.serial.readline()
            
            print "response " + response            

            expected = "90.000," + "360.000" + "\r\n"
     
            print "expected " + expected
            
            if (response in expected):
                break
            else:
                time.sleep(0.001)

        print "done"
        
    def Demo(self):
        response = ""
        
        #while True:
        command = 120
        command = int(command*1000) & 0x00FFFFFF
        command += 0x08000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        
        #command = 720
        #command = int(command*1000) & 0x00FFFFFF
        #command = int(command) & 0x00FFFFFF
        #command += 0x0D000000
        #command = str(command)
        #command += "\n" 
        #self.serial.write(command)
        
         #   command = 0x0E000000
         #   command = str(command)
         #   command += "\n" 
         #   self.serial.write(command)
         #   response = self.serial.readline()
            
         
        #   print "response " + response            

          #  expected = "120.000," + "720.000" + "\r\n"
     
         #   print "expected " + expected
            
          #  if (response in expected):
 #          3
#     break
           # else:
  #              time.sleep(0.01)
        time.sleep(2)
        #while True:
        #command = 120
        #command = int(command*1000) & 0x00FFFFFF
        #command += 0x08000000
        #command = str(command)
        #command += "\n" 
        #self.serial.write(command)
        
        command = 540
        command = int(command*1000) & 0x00FFFFFF
        command = int(command) & 0x00FFFFFF
        command += 0x0D000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
        
        #    command = 0x0E000000
        #    command = str(command)
        #    command += "\n" 
        #    self.serial.write(command)
        #    response = self.serial.readline()
            
        #    print "response " + response            

        #    expected = "120.000," + "540.000" + "\r\n"
     
         #   print "expected " + expected
            
         #   if (response in expected):
         #       break
         #   else:
         #       time.sleep(0.01)
        time.sleep(2)
    
        #while True:
        command = 85
        command = int(command*1000) & 0x00FFFFFF
        command += 0x08000000
        command = str(command)
        command += "\n" 
        self.serial.write(command)
            
        #    command = 0x0E000000
        #    command = str(command)
        #    command += "\n" 
        #    self.serial.write(command)
        #    response = self.serial.readline()
            
        #    print "response " + response            

        #    expected = "85.000," + "540.000" + "\r\n"
     
        #    print "expected " + expected
            
        #    if (response in expected):
        #        break
        #    else:
        #        time.sleep(0.001)

        print "done"
        
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main = Main()
    main.show()
    sys.exit(app.exec_())
